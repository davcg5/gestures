//
//  Tap2ViewController.swift
//  Gestures
//
//  Created by Laboratorio FIS on 17/1/18.
//  Copyright © 2018 DAVID. All rights reserved.
//

import UIKit

class Tap2ViewController: UIViewController {

    @IBOutlet weak var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func swipeUp(_ sender: Any) {
 let action = sender as! UISwipeGestureRecognizer
        customView.backgroundColor = .green
    }
    
    @IBAction func swipeDown(_ sender: Any) {
     customView.backgroundColor = .yellow
    }
    
    
    @IBAction func swipeLeft(_ sender: Any) {
     customView.backgroundColor = .red
    }
    
    @IBAction func swipeRight(_ sender: Any) {
   customView.backgroundColor = .black
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
