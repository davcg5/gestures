//
//  TapViewController.swift
//  Gestures
//
//  Created by Laboratorio FIS on 17/1/18.
//  Copyright © 2018 DAVID. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var coordLabel: UILabel!
    @IBOutlet weak var tapsLabel: UILabel!
    @IBOutlet weak var touchesLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touchesCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        touchesLabel.text = "\(touchesCount ?? 0 )"
        tapsLabel.text = "\(tapCount ?? 0 )"
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("terminal")
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        coordLabel.text = "x: \(x ?? 0), y: \(y ?? 0)"
        print ("x: \(x), y: \(y) ")
    }
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
//    let action = sender as! UITapGestureRecognizer
   tapView.backgroundColor = .red
        
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
