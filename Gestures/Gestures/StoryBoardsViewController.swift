//
//  StoryBoardsViewController.swift
//  Gestures
//
//  Created by Laboratorio FIS on 23/1/18.
//  Copyright © 2018 DAVID. All rights reserved.
//

import UIKit

class StoryBoardsViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let right = tabBar.items![1]
        left.title = "Tap"
        right.title = "Swipe"
        left.image = #imageLiteral(resourceName: "ic_accessibility")
        right.image = #imageLiteral(resourceName: "ic_3d_rotation")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
